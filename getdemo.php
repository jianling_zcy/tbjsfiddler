﻿<?php
    $component = $_GET['component'];
    $callback = $_GET['callback'];

    $currentdir =  dirname(__FILE__);
    $demo_dir = $currentdir.'/demos/';

    $demo = $demo_dir.$component.'.html';

    // 不存在该demo时，将基础库载入
    if(!is_file($demo)){
        $demo = array(
                'html' =>'',
                'css' =>'',
                'javascript' =>'',
                'assets' => '<script type="text/javascript" src="./assets/lib/jquery.js"></script><script type="text/javascript" src="./assets/lib/fis_base.js"></script><script type="text/javascript" src="./assets/lib/module.js"></script>'
            );
        $json = json_encode($demo);
        echo $callback.'('.$json.')';
        return;
    }
    $content = file_get_contents($demo);

    $html = '';
    if(preg_match('/<div[^>]*?>([\s\S]*)<\/div>/', $content, $matchs)){
        $html = $matchs[0];
    }

    $css = '';
    if(preg_match('/<style[^>]*?>([\s\S]*?)<\/style>/', $content, $matchs)){
        $css = $matchs[1];
    }

    $javascript = '';
    if(preg_match_all('/<script.*?>([\s\S]*?)<\/script>/', $content, $matchs)){
        foreach ($matchs[1] as $match) {
            $javascript .= $match;
        }
    }
    if(preg_match_all('/<link.*?>/', $content, $matchs)){
        foreach ($matchs[0] as $match) {
            $assets .= $match;
        }
    }
    if(preg_match_all('/<script.*?src.*?><\/script>/', $content, $matchs)){
        foreach ($matchs[0] as $match) {
            $assets .= $match;
        }
    }
	
	// gbk编码下的中文直接json_encode会不输出，先encode后decode
    $demo = array(
                'html' =>$html,
                'css' =>$css,
                'javascript' =>$javascript,
                'assets' => $assets
            );
	$json = json_encode($demo);
    echo $callback.'('.$json.')';
?>