﻿<?php
    $htmlcode = base64_decode($_POST['html']);
    $csscode = base64_decode($_POST['css']);
    $javascriptcode = base64_decode($_POST['javascript']);
    $assets = base64_decode($_POST['assets']);
    $resource = base64_decode($_POST['resource']);

    $server_base = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
    $server_base = preg_replace('/jsfiddler\/demo.php/', '', $server_base);

    // $csscode = preg_replace('/\.\.\/\.\.\//', $framework_dir, $csscode);
    // $javascriptcode = preg_replace('/\.\.\/\.\.\//', $framework_dir, $javascriptcode);
    $assets = preg_replace('/\.\.\//', '', $assets);

    if(!$htmlcode && !$csscode && !$javascriptcode && !$assets && !$resource)
        return;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>TBUI Demos</title>
        <?php echo $assets; ?>
        <?php echo $resource; ?>
        <?php echo $language; ?>
        <link rel="stylesheet" type="text/css" href="./assets/css/demobeautify.css" />
        <script type="text/javascript" src="./assets/lib/jquery.js"></script>
        <script type="text/javascript" src="./assets/lib/fis_base.js"></script>
        <script type="text/javascript" src="./assets/lib/module.js"></script>
        <style type="text/css">
            <?php echo $csscode; ?>
        </style>
    </head>
    <body>
        <?php echo $htmlcode; ?>
        <script type="text/javascript">
            <?php echo $javascriptcode; ?>
        </script>
        <script type="text/javascript">
            window.Console = window.parent.Console;
        </script>
    <body>
</html>