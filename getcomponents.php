﻿<?php

    $demodir = '../tbui/component/';

    $components = array();
    $handler = opendir($demodir);

    $filename = readdir($handler);

    while($filename){
        if(is_dir($demodir.'/'.$filename) && $filename != '.' && $filename != '..' && !preg_match('/^_/', $filename) && $filename != 'common'){
            array_push($components, $filename);
            
        }
        $filename = readdir($handler);
    }

    echo $callback.'('.json_encode($components).')';
?>