define(function(require, exports) {
    var baidu = require('Tangram').baidu;
    var Editor = require('Editor');
    var Tools = require('Tools');
    var Console = require('Console');
    var Helper = require('Helper');
    var ExternalResource = require('ExternalResource');

    var assets = ''; // 存放demo里面的link和script部分
    var appQuery = {}; // url后面的查询字段，用来自动跳转到某个demo

    var getComponentsAction = './getcomponents.php'; // 获取component列表的接口
    var demoAction = './demo.php'; // 运行demo的页面
    var getDemoAction = 'getdemo.php'; // 获取demo文件各片段的接口

    var initToolbar = function(){
        baidu('#J_run').click(function(){
            Console.clear();
            // onsubmit中有部分逻辑
            baidu('#J_demoForm').submit();
        });

        baidu('#J_runInNewWindow').click(function(){
            baidu('#J_demoForm').attr('target', '_blank');
            baidu('#J_demoForm').submit();
            baidu('#J_demoForm').attr('target', 'demo');
        });

        baidu('#J_reset').click(function(){
            Editor.reset();
        });

        baidu('#J_tidyup').click(function(){
            Editor.tidyup();
        });
    };

    var showComponents = function(){
        var tpl = ['<ul>'];
        baidu.ajax(getComponentsAction, {
            success: function(components){
                components.forEach(function(item){
                    tpl.push('<li data-module="#{item}">#{item}</li>'.replace(/#\{item\}/g, item));
                });

                tpl.push('</ul>');

                baidu('.component-list').html(tpl.join(''));
            },
            dataType: 'jsonp'
        });

        baidu('.component-list').delegate('li', 'click', function(ev){
            var modulename = baidu(ev.target).attr('data-module');

            if(modulename == undefined) return;

            baidu('.component-list li').removeClass('current');
            baidu(ev.target).addClass('current');

            baidu.ajax(getDemoAction + '?component=' + modulename, {
                success: function(data){
                    Editor.setEditorData(data);
                    
                    // 清空控制台
                    Console.clear();
                    assets = data.assets;
                    baidu('#J_run').trigger('click');
                },
                dataType: 'jsonp'
            });
            
        });
    };

    var autoFixIframe = function(){
        baidu('#J_demoIframe').css('width', baidu(window).width() - 230 - 1);
    };

    var prepareSubmit = function(){
        Editor.synchronizeData();

        // var data = Editor.getEditorData();
        var resources = ExternalResource.getEnableResources();
        var imports = [];
        for(var i = 0; i < resources.length; i++){
            /.css$/.test(resources[i].url) ? imports.push('<link rel="stylesheet" type="text/css" href="' + resources[i].url + '" />') : 
                                         imports.push('<script type="text/javascript" src="' + resources[i].url + '"></script>');
        }

        baidu('#J_externalResources').val(imports.join(''));
        baidu('#J_lib').val(baidu('#J_frameworkSelect').val());

        // base64编码
        var htmlField = Editor.getEditor().html.getInputField();
        htmlField.value = Tools.Base64.encode(htmlField.value);

        var cssField = Editor.getEditor().css.getInputField();
        cssField.value = Tools.Base64.encode(cssField.value);
        
        var javascriptField = Editor.getEditor().javascript.getInputField();
        javascriptField.value = Tools.Base64.encode(javascriptField.value);

        var resourceField = baidu('#J_externalResources')[0];
        resourceField.value = Tools.Base64.encode(resourceField.value);

        var assetsField = baidu('#J_assets')[0];
        assetsField.value = Tools.Base64.encode(assets);

    };

    exports.run = function(){
        baidu(window).resize(autoFixIframe);

        Editor.initEditor();
        autoFixIframe();
        initToolbar();
        showComponents();
        ExternalResource.init();
        Console.init();
        // 把Console挂到window下
        window.Console = Console;
        Helper.init();

        baidu('#J_demoForm').submit(function(){
            prepareSubmit();
        });

        
        appQuery = Tools.queryToJson(window.location.href) || {};
        baidu('#J_demoForm').attr('action', demoAction);

        appQuery['lib'] && baidu('#J_frameworkSelect').val(appQuery['lib']);
        baidu('#J_frameworkSelect').trigger('change');
        
    };
});